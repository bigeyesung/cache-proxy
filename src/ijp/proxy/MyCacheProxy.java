package ijp.proxy;
import ijp.Picture;
import ijp.service.Service;
import ijp.service.ServiceFromProperties;
import ijp.utils.Properties;
import ijp.view.View;
import ijp.view.ViewFromProperties;
import java.util.HashMap;
import java.util.HashSet;
import java.util.*;

public class MyCacheProxy implements Service
{
 private Service baseService = null;
 private HashMap<String,Picture> cachemap;   
 public MyCacheProxy() 
  {
	 //implement the Service interface
   baseService = new ServiceFromProperties("CacheProxy.base_service");
   cachemap=new HashMap<String,Picture>();
   
  }
    
	public void RetryProxy(Service baseService) 
  {
   this.baseService = baseService;
  }
	/**
	 * get picture from base
	 */
	public Picture getPicture(String subject, int index) 
  {
	   
	   Picture picture;
	   int b=index;
	   String tem=subject+b;

	 if (cachemap.get(tem)==null)//not in the cachemap   
	  {
		  
		  picture = baseService.getPicture(subject, index);  ///call the service to get pics
         
          String tepo=subject+index;
		  cachemap.put(tepo,picture);
		  return picture;
		  
	//++attempt; 
	  }
	 else  //in the cachemap
	  {
	      Picture picture1= cachemap.get(tem);
	      return picture1;
	  }
	 
  }

	

	@Override
	public String serviceName() {
		// TODO Auto-generated method stub
		return null;
	}
	
	
	
}
