// IJP Assignment 1, Version 5.2.4, 13 Oct 2015
package ijp.test;
//package ijp.proxy;
import static org.junit.Assert.*;
import javafx.scene.image.Image;
import ijp.service.Service;
import ijp.service.ServiceFromProperties;
import org.junit.Test;
import ijp.proxy.*;
import ijp.Picture;
import ijp.service.*;

/**
 * A template for testing a cache proxy for the PictureViewer application.
 * 
 * @author Chenhsi
 * @version YOUR VERSION HERE
 */
public class MyCacheProxyTest implements Service {
	
	/**
	 * Test that consecutive requests return different image.
	 */
	@Test
	public void repeatTest() {

		CacheProxy1 proxy = new CacheProxy1();
		Picture firstPicture = proxy.getPicture("A",1);
		Picture secondPicture = proxy.getPicture("B",2);
		Picture thirdPicture = proxy.getPicture("C",3);
		Picture forthPicture = proxy.getPicture("D",4);
		Picture fifthPicture = proxy.getPicture("E",5);
		Picture sixthPicture = proxy.getPicture("A",1);
		Picture senventhPicture = proxy.getPicture("B",2);
		Picture eightthPicture = proxy.getPicture("C",3);

	
		assertTrue(
				"different picture returned for same subject (and index)",
				firstPicture.equals(sixthPicture));
	}
	/**
	 * Test that requests for the same subject and index return the same image in MyCacheProxy
	 */
	@Test
	public void equalityTest() {

		MyCacheProxy proxy = new MyCacheProxy();
		Picture firstPicture = proxy.getPicture("equalityTest",2);
		Picture secondPicture = proxy.getPicture("equalityTest",2);
		assertTrue(
				"different picture returned for same subject (and index)",
				firstPicture.equals(secondPicture));
	}
	/**
	 * Test that requests for the different index return the different image in MyCacheProxy
	 */
    @Test
	public void indexTest(){
    	
		MyCacheProxy proxy = new MyCacheProxy();
		Picture firstPicture = proxy.getPicture("testA",1);
		Picture secondPicture = proxy.getPicture("testA",1);
		firstPicture.index();
		secondPicture.index();
		assertEquals(firstPicture.index(), secondPicture.index());
		
		
	}
	/**
	 * Test that requests for the same subject and index return the same image in CacheProxy1
	 */
    @Test
	public void equalityTest1() {

		CacheProxy1 proxy = new CacheProxy1();
		Picture firstPicture = proxy.getPicture("equalityTest",2);
		Picture secondPicture = proxy.getPicture("equalityTest",2);
		assertTrue(
				"different picture returned for same subject (and index)",
				firstPicture.equals(secondPicture));
	}
	/**
	 * Test that requests for the different index return the different image in CacheProxy1
	 */
	 @Test
		public void indexTest1(){
	    	
			CacheProxy1 proxy = new CacheProxy1();
			Picture firstPicture = proxy.getPicture("testA",2);
			Picture secondPicture = proxy.getPicture("testA",1);
			assertEquals( firstPicture, secondPicture );
		}
	 
	/**
    * Test that requests for the different subject return the different image in CacheProxy1
	*/
    @Test
	public void subjectTest1(){
		CacheProxy1 proxy = new CacheProxy1();
		Picture firstPicture = proxy.getPicture("TestA",2);
		Picture secondPicture = proxy.getPicture("TestB",2);
		assertTrue(
				"different picture returned for different index)",
				firstPicture.equals(secondPicture));	
		
	}	
	/**
     * Test that requests for default value in CacheProxy1
 	*/
    @Test
	public void valid1(){
		CacheProxy1 proxy = new CacheProxy1(this);
		Picture firstPicture = proxy.getPicture("TestA",2);
		assertTrue(  firstPicture.isValid() );	
	}	
	/**
     * Test that requests for default value in CacheProxy2
 	*/
    @Test
	public void valid2(){
		CacheProxy2 proxy = new CacheProxy2(this);
		Picture firstPicture = proxy.getPicture("TestA",2);
		assertTrue(  firstPicture.isValid() );	
	}	
	/**
     * Test that requests for default value in CacheProxy3
 	*/
    @Test
	public void valid3(){
		CacheProxy3 proxy = new CacheProxy3(this);
		Picture firstPicture = proxy.getPicture("TestA",2);
		assertTrue(  firstPicture.isValid() );	
	}	
    
	/**
	 * Test that requests for the different index return the different image in CacheProxy2
	 */
    @Test
	public void indexTest2(){
    	
		CacheProxy2 proxy = new CacheProxy2();
		Picture firstPicture = proxy.getPicture("testA",2);
		Picture secondPicture = proxy.getPicture("testA",1);
		assertEquals( firstPicture, secondPicture );
	}

	/**
     * Test that requests for the different subject return the different image in CacheProxy2
 	*/
    @Test
    public void subjectTest2(){
	    CacheProxy2 proxy = new CacheProxy2();
	    Picture firstPicture = proxy.getPicture("TestA",2);
	    Picture secondPicture = proxy.getPicture("TestB",2);
	    assertTrue(
			"different picture returned for different index)",
			firstPicture.equals(secondPicture));	
    }
	/**
	 * Test that requests for the different index return the different image in CacheProxy3
	 */
    @Test
   	public void indexTest3(){
       	
   		CacheProxy3 proxy = new CacheProxy3();
   		Picture firstPicture = proxy.getPicture("testA",2);
   		Picture secondPicture = proxy.getPicture("testA",1);
   		assertEquals( firstPicture, secondPicture );
   	}
	/**
	 * Test that requests for the different subject return the different image in CacheProxy3
	 */
       @Test
       public void subjectTest3(){
   	    CacheProxy3 proxy = new CacheProxy3();
   	    Picture firstPicture = proxy.getPicture("TestA",2);
   	    Picture secondPicture = proxy.getPicture("TestB",2);
   	    assertTrue(
   			"different picture returned for different index)",
   			firstPicture.equals(secondPicture));	
       }	
    
    
	
	
	/**
	 * Return a picture from the simulated service.
	 * This service simply returns an empty picture every time that it called.
	 * Note that a <em>different</em> object is returned each time, even if the
	 * subject and index are the same.
	 *
	 * @param subject the requested subject
	 * @param index the index of the picture within all pictures for the requested topic
	 *
	 * @return the picture
	 */
	@Override
	public Picture getPicture(String subject, int index) {
		return (new UnreliableProxy()).getPicture(subject, index);
		//return new Picture((Image)null, subject ,serviceName(), index);
	}
	
	/**
	 * Return a textual name to identify the simulated service.
	 *
	 * @return the name of the service ("cacheProxyTest")
	 */
	@Override
	public String serviceName() {
		return "MycacheProxyTest";
	}
}
