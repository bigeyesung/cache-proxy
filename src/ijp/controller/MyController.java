// IJP Assignment 1, Version 5.2.4, 13 Oct 2015
package ijp.controller;

import ijp.Picture;
import ijp.service.Service;
import ijp.service.ServiceFromProperties;
import ijp.utils.Properties;
import ijp.view.View;
import ijp.view.ViewFromProperties;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Scanner;
/**
 * A template for implementing a controller for the PictureViewer application.
 * 
 * @author Chenhsi
 * @version YOUR VERSION HERE
 */
public class MyController implements Controller {

	private View view;
	private Service service;
	private HashMap<Integer,String> index;
	
	public MyController()
	{
	   index=new HashMap<Integer,String>();
	}
	
    
	/**
	 * Start the controller.
	 */
	public void start() 
	{

		// create the view and the service objects
		view = new ViewFromProperties(this);
		service = new ServiceFromProperties();
		//public static java.lang.String get(java.lang.String propertyName)
        String temporary=Properties.get( "MyController.subjects");
        //String inputLine=temporary.trim();
        String[] wordArray=temporary.split(",");
        
        //String inputLine=temporary.trim();
        HashSet<String> words=new HashSet<String>();
        for(String word:wordArray)
        {   word.trim();
        	words.add(word.trim());
        }

		// create two selections in the interface
        for(String word:words)
        {
           
        	int selection = view.addSelection(word);
        	index.put(selection, word);
        }
 
		// start the interface
		view.start();
	}
        //add name
	    public void addSubject(String name)
	{
	    int selection = view.addSelection(name);
		index.put(selection,name);
		
	}
	
	
	/**
	 * Handle the specified selection from the interface.
	 *
	 * selectionID the id of the selected item
	 */
	public void select(int selection) 
	{
		
		// a picture corresponding to the selectionID
		// by default, this is an empty picture
		// (this is used if the selectionID does not match)
		
		Picture picture = null;
		String word = index.get(selection);
		// create a picture corresponding to the selectionID
        //selectionID=index.get(name);
		picture = service.getPicture(word,1);

		view.showPicture(picture);
	}
}
