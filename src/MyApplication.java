// IJP Assignment 1, Version 5.2.4, 13 Oct 2015
import ijp.PictureViewer;
 
/**
 * The main program for launching a PictureViewer application.
 *
 * @author  Paul Anderson &lt;dcspaul@ed.ac.uk&gt;
 * @version 5.2.4, 13 Oct 2015
 */
public class MyApplication {
       
	public static void main(String args[]){
		PictureViewer.main(args);
	}
}
